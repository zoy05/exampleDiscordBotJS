const DiscordAPI = require("discord.js");
const settings = require("../settings.json");
const colors = require('../colors.json')

module.exports.run = async (bot, message, args) => {
    let typeServer = "error";
    if (message.guild.memberCount <= 50) {
        typeServer = "small";
        }
    if (message.guild.memberCount >= 50 && message.guild.memberCount <= 300) {
        typeServer = "medium";
    }
    if (message.guild.memberCount >= 300) {
        typeServer = "large";
    }

    let sicon = message.guild.iconURL;
    let serverembed = new DiscordAPI.RichEmbed()
    .setDescription("Server Information")
    .setColor(colors.purple)
    .setThumbnail(sicon)
    .setTitle(message.guild.name)
    .addField("Created On:", message.guild.createdAt)
    .addField("You Joined:", message.member.joinedAt)
    .addField("Total Members:", message.guild.memberCount, true)
    .addField("Server Type:", typeServer, true)
    .setFooter("Zoy©", bot.user.displayAvatarURL);
    message.channel.send(serverembed);
};

module.exports.help = {
  name:"serverinfo"
};
